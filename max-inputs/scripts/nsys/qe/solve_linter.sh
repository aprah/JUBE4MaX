#!/bin/bash
nsys  profile --trace=cuda,nvtx,mpi,openacc --capture-range=nvtx --nvtx-capture='solve_linter' --capture-range-end=repeat[:1] --env-var=NSYS_NVTX_PROFILER_REGISTER_ONLY=0 --kill='none' $@
