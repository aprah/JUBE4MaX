#!/bin/bash
#from: Louis Stuber
#usage: srun ./binder.sh $0 ./a.out
#then ./memory-parser.sh $SLURM_JOBID
# Get MPI rank/size using SLURM and/or OpenMPI env variables.
if [[ -z $OMPI_COMM_WORLD_LOCAL_RANK ]]; then
   export OMPI_COMM_WORLD_RANK=$SLURM_PROCID
   export OMPI_COMM_WORLD_LOCAL_RANK=$SLURM_LOCALID
   export OMPI_COMM_WORLD_SIZE=$SLURM_NTASKS
   export OMPI_COMM_WORLD_LOCAL_SIZE=$((OMPI_COMM_WORLD_SIZE/SLURM_NNODES))
fi
if [[ -z $OMPI_COMM_WORLD_LOCAL_RANK ]]; then
   export OMPI_COMM_WORLD_RANK=$PMI_RANK
   export OMPI_COMM_WORLD_LOCAL_RANK=$MPI_LOCALRANKID
   export OMPI_COMM_WORLD_SIZE=$PMI_SIZE
   export OMPI_COMM_WORLD_LOCAL_SIZE=$MPI_LOCALNRANKS
fi
if [[ -z $OMPI_COMM_WORLD_LOCAL_RANK ]]; then
   echo "$0 Error: OMPI_COMM_WORLD_LOCAL_RANK not defined. This script only supports SLURM and/or OpenMPI."
   exit 101
fi
if [[ -z $OMPI_COMM_WORLD_LOCAL_SIZE ]]; then
   if [[ -n $LSUB_NTASKS_PER_NODE ]]; then
      OMPI_COMM_WORLD_LOCAL_SIZE=$LSUB_NTASKS_PER_NODE
   else
      echo "$0 error: OMPI_COMM_WORLD_LOCAL_SIZE not defined."
      exit 102
   fi
fi
if [[ $OMPI_COMM_WORLD_LOCAL_RANK == 0 ]];then
   mkdir -p tmpmem
   nvidia-smi --loop-ms=100 --format=csv,noheader --query-gpu=memory.used > tmpmem/tmpgpumem.$SLURM_JOBID.$(hostname).txt &
   pid0=$!
   (while true; do top -b -n 1 | head -20; sleep 0.1; done) > tmpmem/tmptopmem.$SLURM_JOBID.$(hostname).txt &
   pid1=$!
   (while true; do free; sleep 0.1; done) > tmpmem/tmpfreemem.$SLURM_JOBID.$(hostname).txt &
   pid2=$!
fi
$@
if [[ $OMPI_COMM_WORLD_LOCAL_RANK == 0 ]];then
kill $pid0
kill $pid1
kill $pid2
fi
sleep 20
if [[ $OMPI_COMM_WORLD_RANK == 0 ]]; then
   if [[ -n $SLURM_JOBID ]]; then
      SUFFIX="$SLURM_JOBID.txt"
   else
      SUFFIX="txt"
   fi
   rm -f tmpmem/tmpusedmem.$SUFFIX
   rm -f tmpmem/tmpavailmem.$SUFFIX
   rm -f tmpmem/tmpusedgpumem.$SUFFIX
   maxusedmem=0
   minavailmem=200000000000
   maxusedgpumem=0
   for file in tmpmem/tmpfreemem.$SLURM_JOBID.*; do
      usedmem=$(cat $file | awk '{print $3}' | sort -n | tail -1)
      echo "$usedmem" >> tmpmem/tmpusedmem.$SUFFIX
      availmem=$(cat $file | awk '{print $7}' | sort -n | sed '/^$/d' | head -1)
      echo "$availmem" >> tmpmem/tmpavailmem.$SUFFIX
      if [[ "$usedmem" -gt $maxusedmem ]]; then
         maxusedmem="$usedmem"
         imaxusedmem=$file
      fi
      if [[ "$availmem" -lt $minavailmem ]]; then
         minavailmem="$availmem"
         iminavailmem=$file
      fi
   done
   for file in tmpmem/tmpgpumem.$SLURM_JOBID.*; do
      usedgpumem="$(cat $file | sort -n | tail -1)"
      echo "$usedgpumem" >> tmpmem/tmpusedgpumem.$SUFFIX
      if [[ "$usedgpumem" > "$maxusedgpumem" ]]; then
         maxusedgpumem="$usedgpumem"
         imaxusedgpumem=$file
      fi
   done
   echo "CPU ==="
   echo "Max used (res) mem = $maxusedmem kB (file $imaxusedmem)"
   echo "Min avail mem      = $minavailmem kB (file $iminavailmem)"
   echo "GPU ==="
   echo "Max used GPU mem = $maxusedgpumem (file $imaxusedgpumem)"
fi
