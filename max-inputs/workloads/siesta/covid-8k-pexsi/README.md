Benchmark for sars-cov-2 protein, using the PEXSI solver

Note that the "magic numbers" of tasks-per-pole and number of poles
are coupled to the actual number of cores used in the job. Hence, the
.fdf file should really be a template to be pre-processed for each case.
